//
//  ItemDetailsViewControllerTests.swift
//  SEPHORATests
//
//  Created by Maher Ayari on 6/10/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

@testable import SEPHORA
import XCTest

class ItemDetailsViewControllerTests: XCTestCase {
    private var viewController: ItemDetailsViewController?
    private var viewModel: ItemDetailsViewModel?
    
    override func setUp() {
        super.setUp()
        let item = Item()
        item.itemDescription = "description"
        item.location = "location"
        viewModel = ItemDetailsViewModel(item: item)
        viewController = ItemDetailsViewController(viewModel: viewModel!)
    }
    
    override func tearDown() {
        viewController = nil
        super.tearDown()
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testFields() {
        XCTAssertEqual(viewModel?.item.location, "location")
        XCTAssertEqual(viewModel?.item.itemDescription, "description")
    }
    
}
