//
//  AppDelegate.swift
//  SEPHORA
//
//  Created by Maher Ayari on 06/08/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  var appCoordinator: AppCoordinator?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
    startAppCordinator()
    return true
  }
}

extension AppDelegate {
    func startAppCordinator() {
        let dependancyManager = DependencyManager()
        self.appCoordinator = AppCoordinator(dataManagerFactory: dependancyManager.dataManagerFactory)
        self.appCoordinator?.start()
    }
}

