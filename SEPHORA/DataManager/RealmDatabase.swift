//
//  RealmDatabase.swift
//  SEPHORA
//
//  Created by Maher Ayari on 06/08/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import Foundation
import RealmSwift
import RxRealm

public struct DataManagerError: LocalizedError {
    enum Reason {
        case databaseFileNotFound
    }
    
    let reason: Reason
    
    init(reason: Reason) {
        self.reason = reason
    }
}

open class RealmDatabase {
    
    
    public typealias DatabaseObject = Object
    public typealias DatabaseResult = Results<Object>
    
    private let activeConfiguration: Realm.Configuration
    
    public init(configuration: Realm.Configuration) {
        self.activeConfiguration = configuration
    }
    
    func loadOrCreateRealm() throws {
        do {
            _ = try Realm(configuration: self.activeConfiguration)
        } catch {
            try RealmDatabase.deleteRealmFile()
            _ = try Realm(configuration: self.activeConfiguration)
        }
        
        print("Realm file url: \(String(describing: Realm.Configuration.defaultConfiguration.fileURL))")
    }
    
    static func deleteRealmFile() throws {
        guard let databaseFileUrl = Realm.Configuration.defaultConfiguration.fileURL else {
            let errorMessage = "RealmDatabase - Unable to retrieve Realm fileURL for deletion"
            print(errorMessage)
            throw DataManagerError(reason: .databaseFileNotFound)
        }
        
        try FileManager.default.removeItem(at: databaseFileUrl)
    }
    
    func realmDB() -> Realm {
        do {
            return try Realm(configuration: activeConfiguration)
        } catch (let error) {
            fatalError("Realm DB not available: \(error)")
        }
    }
    
    public func get<T: DatabaseObject>(_ object: T.Type) -> Results<T> {
        let realm = realmDB()
        let result = realm.objects(object)
        realm.refresh() 
        return result
    }
    
    public func insert<T, S: Sequence>(_ objects: S, ofType: T.Type, update: Bool = true) throws where S.Iterator.Element: AnyObject, T: Object {
        guard let realmObjects = objects as? [T] else { fatalError("Elements of sequence have to be a subclass of Realm.Object") }

        let realm = realmDB()

        let change: () -> Void = {
      update ? realm.add(realmObjects, update: Realm.UpdatePolicy.all) : realm.add(realmObjects)
        }

        do {
            try isInTransaction() ? change() : wrapInTransaction(change)
        } catch {
            throw error
        }
    }
    
    fileprivate func wrapInTransaction(_ writeBlock: @escaping () -> Void) throws {
        let realm = realmDB()
        do {
            try realm.write {
                writeBlock()
            }
        } catch {
            throw error
        }
    }
    
    fileprivate func isInTransaction() -> Bool {
        let realm = realmDB()
        return realm.isInWriteTransaction
    }
    
    public func beginTransaction() {
        let realm = realmDB()
        if realm.isInWriteTransaction {
            realm.cancelWrite()
        }
        realm.beginWrite()
    }
    
    public func commitTransaction() throws {
        let realm = realmDB()
        do {
            try realm.commitWrite()
        } catch {
            throw error
        }
    }
    
    public func cancelTransaction() {
        let realm = realmDB()
        if realm.isInWriteTransaction {
            realm.cancelWrite()
        }
    }
    
    
}
