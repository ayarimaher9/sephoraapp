//
//  DependencyManager.swift
//  SEPHORA
//
//  Created by Maher Ayari on 06/08/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import Foundation
import RealmSwift

public class DependencyManager {
    
    private let groheNetwork = SephoraAPI()
    
    private var database: RealmDatabase
    public let dataManagerFactory: DataManagerFactory
    
    public init() {
        let configuration = Realm.Configuration()
        self.database = RealmDatabase(configuration: configuration)
        self.dataManagerFactory = DataManagerFactory(database: database, sephoraNetwork: groheNetwork)
        loadOrCreateDatabase()
    }
    
    public func loadOrCreateDatabase() {
        do {
            try self.database.loadOrCreateRealm()
        } catch let error {
            let errorMessage = "DependencyManager - Unable to create database instance. Failed with: " + error.localizedDescription
            print(errorMessage)
            fatalError(errorMessage)
        }
    }
}
