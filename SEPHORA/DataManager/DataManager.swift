//
//  DataManager.swift
//  SEPHORA
//
//  Created by Maher Ayari on 6/9/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import Foundation
import RealmSwift
import RxRealm
import RxSwift

public typealias SephoraResponse = Any


public class DataManager {
    // MARK: - Properties
    var disposeBag = DisposeBag()
    let network: SephoraAPI
    let database: RealmDatabase
    
    // MARK: - Init
    public init(network: SephoraAPI, database: RealmDatabase) {
        self.network = network
        self.database = database
    }
    
    public func getItems() -> Results<Item> {
        return database.get(Item.self)
    }
    
}
