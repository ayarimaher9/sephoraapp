//
//  ItemDataManager.swift
//  SEPHORA
//
//  Created by Maher Ayari on 6/10/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import Foundation

public protocol ItemDataManager {
}

public class ItemManager: DataManager, ItemDataManager {
    
    public override init(network: SephoraAPI, database: RealmDatabase) {
        super.init(network: network, database: database)
    }
    
}

