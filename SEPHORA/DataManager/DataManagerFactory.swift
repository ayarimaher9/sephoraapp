//
//  DataManagerFactory.swift
//  SEPHORA
//
//  Created by Maher Ayari on 06/08/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import Foundation

public class DataManagerFactory {
    
    private let realmDatabase: RealmDatabase
    public let sephoraNetwork: SephoraAPI
    
    
    public init(database: RealmDatabase, sephoraNetwork: SephoraAPI) {
        self.realmDatabase = database
        self.sephoraNetwork = sephoraNetwork
    }
    
    public lazy var itemDataManager: ItemManager = {
      return ItemManager(network: sephoraNetwork, database: realmDatabase)
    }()
}
