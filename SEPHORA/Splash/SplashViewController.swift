//
//  SplashViewController.swift

//
//  Created by Maher Ayari on 06/08/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    var viewModel: SplashViewModel
    
    public init(viewModel: SplashViewModel) {
        self.viewModel = viewModel
        super.init(nibName: String(describing: type(of: self)), bundle: nil)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

  override func viewDidLoad() {
        super.viewDidLoad()
    }
    
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    _ = Timer.scheduledTimer(withTimeInterval: 0.7, repeats: false, block: {_ in
        self.viewModel.checkUserLogin()
    })
  }
}
