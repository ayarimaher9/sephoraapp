//
//  SplashViewModel.swift
//
//
//  Created by Maher Ayari on 06/08/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import Foundation

protocol SplashViewModelDelegate: class {
    func goToDashboard()
}

class SplashViewModel: BaseViewModel {
    var delegate: SplashViewModelDelegate?
    
    public func checkUserLogin(){
        self.delegate?.goToDashboard()
        
    }
}
