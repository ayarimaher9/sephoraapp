//
//  Item.swift
//  SEPHORA
//
//  Created by Maher Ayari on 6/9/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

public class Items: Object, Mappable {
    public var items = List<Item>()
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        items <- (map["items"], ArrayTransform<Item>())
    }
}

public class Item: Object, Mappable {
    @objc public dynamic var id = ""
    @objc public dynamic var itemDescription = ""
    @objc public dynamic var location = ""
    @objc public dynamic var image = ""
    
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        id <- map["id"]
        itemDescription <- map["description"]
        location <- map["location"]
        image <- map["image"]
    }
}

class ArrayTransform<T: RealmSwift.Object> : TransformType where T: Mappable {
    
    internal typealias Object = List<T>
    internal typealias JSON = [Any]
    
    let mapper = Mapper<T>()
    var idFromParent: String?
    
    init() {}
    
    convenience init(idFromParent: String) {
        self.init()
        self.idFromParent = idFromParent
    }
    
    func transformFromJSON(_ value: Any?) -> List<T>? {
        let result = List<T>()
        if let tempArr = value as? [Any]? {
            tempArr!.enumerated().forEach { _, element in
                let mapper = Mapper<T>()
                let model: T? = mapper.map(JSONObject: element)
                if let model = model {
                    if let idFromParent = idFromParent,
                       let model = model as? ParentIdSetable {
                        model.setIdFromParent(idFromParent + "_\(String(describing: type(of: T.self)))")
                    }
                    result.append(model)
                } else {
                    fatalError("Could not map model")
                }
            }
        }
        return result
    }
    
    func transformToJSON(_ value: Object?) -> JSON? {
        var results = [Any]()
        if let value = value {
            for obj in value {
                let json = mapper.toJSON(obj)
                results.append(json)
            }
        }
        return results
    }
}

public protocol ParentIdSetable {
    func setIdFromParent(_ id: String)
}
