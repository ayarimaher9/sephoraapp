//
//  Networking.swift
//  SEPHORA
//
//  Created by Maher Ayari on 6/9/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import Alamofire
import ObjectMapper
import Moya
import RxCocoa
import RxSwift

public enum APIEndpoint {
    case getItems
}

extension APIEndpoint: TargetType {
    
    public var sampleData: Data {
      fatalError("Don't use this method. Use the APISampleReponses.")
    }
    
    public var headers: [String: String]? {
        switch self {
        case .getItems:
            return self.defaultHeaders()
        }
    }
    
    public var baseURL: URL {
        switch self {
        default:
            guard let baseURL = URL(string: "https://sephoraios.github.io/items.json") else { fatalError("The url is not valid") }
            return baseURL
        }
    }
    
    public var path: String {
        switch self {
        default:
            return ""
        }
    }
    
    public var method: Moya.Method {
        switch self {
        default:
            return .get
        }
    }
    
    public var task: Task {
        switch self {
        default:
            return .requestParameters(parameters: [:], encoding: JSONEncoding())
        }
    }
    
    private func defaultHeaders() -> [String: String] {
        return ["Content-type": "application/json"]
    }
    
}

class GroheAPISessionDelegate: SessionDelegate {
    
    // Prevent redirects
    override func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Void) {
        completionHandler(nil)
    }
}

open class SephoraAPI: SephoraNetwork {
    
    // MARK: - Properties
    public var moyaProvider: MoyaProvider<APIEndpoint>?
    
    internal let disposeBag = DisposeBag()
    
    // MARK: - Init
    public init() {
        
//        let urlSessionConfig = URLSessionConfiguration.default
//        urlSessionConfig.requestCachePolicy = .reloadIgnoringCacheData
//        urlSessionConfig.urlCache = nil
//        urlSessionConfig.shouldUseExtendedBackgroundIdleMode = true
//        
//        let stm = ServerTrustManager(allHostsMustBeEvaluated: false, evaluators: [:])
//
//        let session = Alamofire.Session(
//            configuration: urlSessionConfig,
//            delegate: GroheAPISessionDelegate(),
//            serverTrustManager: stm)
//        
//        moyaProvider = MoyaProvider<APIEndpoint>(
//            endpointClosure: self.getEntpointClosure(),
//            requestClosure: { endpoint, closure in
//                do {
//                    var request = try endpoint.urlRequest()
//                    request.cachePolicy = .reloadIgnoringCacheData
//                    closure(.success(request))
//                } catch let error {
//                    closure(.failure(MoyaError.underlying(error, nil)))
//                }
//            },
//            session: session,
//            plugins: [])
    }
    
    public func getEntpointClosure() -> ((APIEndpoint) -> Endpoint) {
        return { [] (target: APIEndpoint) -> Endpoint in
            
            return MoyaProvider.defaultEndpointMapping(for: target)
        }
    }
    
    public func url(_ route: TargetType) -> String {
      // Prevent add a slash on URL end if using empty paths
      if route.path.isEmpty {
        return route.baseURL.absoluteString
      }

      return route.baseURL.appendingPathComponent(route.path).absoluteString
    }
    
    public func getItems() -> Observable<Response> {
        return moyaRequest(.getItems)
    }
    
    // MARK: - Private
    private func moyaRequest(_ request: APIEndpoint) -> Observable<Response> {
        guard let moyaProvider = moyaProvider else { fatalError("MoyaProvider is not available") }

    return  moyaProvider.rx.request(request).asObservable()
    }
    
    func send<T>(_ request: T) -> Observable<[Item]> {

        let request = URLRequest(url: URL(string: "https://sephoraios.github.io/items.json")!)

        return Observable.create { obs in
            URLSession.shared.rx.response(request: request).debug("r").subscribe(
                onNext: { response in
                    do {
                        let json = try JSONSerialization.jsonObject(with: response.data, options: .allowFragments)
                        let items = Mapper<Items>().map(JSON: json as! [String : Any])
                        return obs.onNext(((items?.items.toArray()))!)
                        
                    } catch {
                        print("json error: \(error)")
                    }
                    
            },
                onError: {error in
                    obs.onError(error)
            })
        }
    }
}

