//
//  TabBarViewController.swift
//  SEPHORA
//
//  Created by Maher Ayari on 6/9/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import Foundation
import UIKit

class `TabBarViewController`: UITabBarController {

  let dataManagerFactory: DataManagerFactory
  // MARK: Properties
  let navigationCoordinator: NavigationCoordinator
  // MARK: Init
  init(navigationCoordinator: NavigationCoordinator, dataManagerFactory: DataManagerFactory) {
    self.navigationCoordinator = navigationCoordinator
    self.dataManagerFactory = dataManagerFactory
    super.init(nibName: nil, bundle: nil)
  }
  
  public required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
        super.viewDidLoad()
        setUpTabBar()
    }
  
  func setUpTabBar() {
    
    // Adjust tabbar icon positions
    let imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
    tabBar.isTranslucent = true
    // Set item colors
    tabBar.tintColor = UIColor.red
    tabBar.unselectedItemTintColor = UIColor.red
    // Set  background
    tabBar.barTintColor = UIColor.white
    tabBar.backgroundColor = UIColor.white
    // Remove top border
    tabBar.shadowImage = UIImage()
    tabBar.backgroundImage = UIImage()
    
    
    let productListCoordinator = ProductListCoordinator(navigationCoordinator: navigationCoordinator, dataManagerFactory: dataManagerFactory)
   
    
    navigationCoordinator.subCoordinators.append(productListCoordinator)
   
    
    let vcProductList = productListCoordinator.viewController()
    vcProductList.tabBarItem.title = ""
    vcProductList.tabBarItem.imageInsets = imageInsets
    vcProductList.tabBarItem.image = UIImage(named: "share")
    vcProductList.tabBarItem.selectedImage = UIImage(named: "share")
  
    viewControllers = [vcProductList]

  }
    

}
