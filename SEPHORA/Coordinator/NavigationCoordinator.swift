//
//  NavigationCoordinator.swift
//  SEPHORA
//
//  Created by Maher Ayari on 6/9/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import Foundation
import UIKit

class NavigationCoordinator: BaseCoordinator {
    var window: UIWindow?
    
    // MARK: - Properties
    private var navigationController: UINavigationController?
    private var tabBarController: TabBarViewController?
    
    public  init(dataManagerFactory: DataManagerFactory) {
        super.init(identifier: .navigation, dataManagerFactory: dataManagerFactory)
    }
    
    public override func start() {
        
        tabBarController = TabBarViewController(navigationCoordinator: self, dataManagerFactory: dataManagerFactory)
        
        guard let tabBarController = tabBarController else { return }
        
        navigationController = UINavigationController(rootViewController: tabBarController)
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        guard let navigationController = navigationController else { return }
        
        UIApplication.shared.delegate?.window??.rootViewController = navigationController
        UIApplication.shared.delegate?.window??.makeKeyAndVisible()
    }
    
    public func pushViewController(viewController: UIViewController, hideTabBar: Bool) {
        
        guard let selectedIndex = tabBarController?.selectedIndex else { return }
        if let currentNavigationController = tabBarController?.viewControllers?[selectedIndex] as? UINavigationController {
            if let lastViewController = currentNavigationController.viewControllers.last {
                viewController.hidesBottomBarWhenPushed = hideTabBar
                if let vc = viewController as? UINavigationController {
                    lastViewController.navigationController?.present(vc, animated: true, completion: nil)
                } else {
                    lastViewController.navigationController?.pushViewController(viewController, animated: true)
                }
            } else {
                fatalError("WMNavigationCoordinator - pushViewToCurrentTabBarViewController(): TabBar view last controller not found!")
            }
        } else {
            fatalError("WMNavigationCoordinator - pushViewToCurrentTabBarViewController(): TabBar view has no navigation controller!!")
        }
    }
}
