//
//  AppCoordinator.swift
//  SEPHORA
//
//  Created by Maher Ayari on 06/08/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import Foundation
import UIKit

public class AppCoordinator: BaseCoordinator {
    public var window: UIWindow?
    public var navigationController: UINavigationController?
    
    public init?(dataManagerFactory: DataManagerFactory) {
        super.init(identifier: .app, dataManagerFactory: dataManagerFactory)
        self.navigationController = UINavigationController()

    }
  
  public override func start() {
    // MARK: Start Splash
    let splashCoordinator = SplashCoordinator(dataManagerFactory: dataManagerFactory)
    self.navigationController = UINavigationController()
    splashCoordinator.window = window
    splashCoordinator.start()
    subCoordinators.append(splashCoordinator)
    
  }
  
}
