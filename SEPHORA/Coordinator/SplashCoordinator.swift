//
//  SplashCoordinator.swift
//  SEPHORA
//
//  Created by Maher Ayari on 6/10/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import Foundation
import UIKit

class SplashCoordinator: BaseCoordinator {
    
    // MARK: - Properties
    private var navigationController: UINavigationController?
    var window: UIWindow?
    

    private lazy var splashViewModel : SplashViewModel = {
           let splashViewModel = SplashViewModel()
           splashViewModel.delegate = self
           return splashViewModel
       }()
    
    
    public init(dataManagerFactory: DataManagerFactory) {
        super.init(identifier: .splash, dataManagerFactory: dataManagerFactory)
        self.navigationController = UINavigationController()
    }
    
    public override func start(){
        let splashViewController = SplashViewController(viewModel: splashViewModel)
        self.navigationController?.viewControllers = [splashViewController]
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = self.navigationController
        self.window?.makeKeyAndVisible()
    }

    
    private func startNavigationCordinator(){
        // Init tab bar navigation controller
        let navigationCoordinator = NavigationCoordinator(dataManagerFactory: dataManagerFactory)
        navigationCoordinator.window = window
        navigationCoordinator.start()
        subCoordinators.append(navigationCoordinator)
    }

}

extension SplashCoordinator: SplashViewModelDelegate {
    func goToDashboard() {
        self.startNavigationCordinator()
    }
}


