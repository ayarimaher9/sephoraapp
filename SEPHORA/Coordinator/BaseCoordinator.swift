//
//  BaseCoordinator.swift
//  SEPHORA
//
//  Created by Maher Ayari on 6/9/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import Foundation

public class BaseCoordinator: Coordinator {
    public var identifier: CoordinatorType
    
    // MARK: -Properties
    let dataManagerFactory: DataManagerFactory
    var subCoordinators = [Coordinator]()
    var parentCoordinator: Coordinator?
    
    // MARK: -Init
    public init(identifier: CoordinatorType, dataManagerFactory: DataManagerFactory) {
        self.identifier = identifier
        self.dataManagerFactory = dataManagerFactory
    }
        
    public func start() {
        fatalError("Implement in subcalss")
    }
}
