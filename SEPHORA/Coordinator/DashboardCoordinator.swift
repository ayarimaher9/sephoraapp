//
//  ProductListCoordinator.swift
//  SEPHORA
//
//  Created by Maher Ayari on 06/08/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import Foundation
import UIKit

class ProductListCoordinator: BaseCoordinator {
    var window: UIWindow?
    var navigationCoordinator: NavigationCoordinator
    
    private lazy var productListViewModel: ProductListViewModel = {
      let productListViewModel = ProductListViewModel(dataManagerFactory: dataManagerFactory)
      productListViewModel.productListDelegate = self
      return productListViewModel
    }()
    
    public init(navigationCoordinator: NavigationCoordinator, dataManagerFactory: DataManagerFactory) {
        self.navigationCoordinator = navigationCoordinator
        super.init(identifier: .dashboard, dataManagerFactory: dataManagerFactory)
    }
    
    public func viewController() -> UIViewController {
        let productListViewController = ProductListViewController(viewModel: productListViewModel)
        productListViewController.ProductListCoordinator = self
        let navigation = UINavigationController(rootViewController: productListViewController)
        return navigation
    }
    
    public func goToItemDetails(item: Item) {
        let viewModel = ItemDetailsViewModel(item: item)
        let viewController = ItemDetailsViewController(viewModel: viewModel)
        navigationCoordinator.pushViewController(viewController: viewController, hideTabBar: true)
    }
    
}
