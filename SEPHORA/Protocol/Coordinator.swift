//
//  Coordinator.swift
//  SEPHORA
//
//  Created by Maher Ayari on 6/9/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import Foundation

public enum CoordinatorType: String {
    case app
    case splash
    case dashboard
    case navigation
}

public protocol Coordinator {
    var identifier: CoordinatorType { get }
    // start coordinator flow
    func start()
}

public protocol CoordinatorDelegate: class {}
