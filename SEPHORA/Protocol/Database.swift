//
//  Database.swift
//  SocialNetwork
//
//  Created by Maher Ayari on 10/30/20.
//  Copyright © 2020 Baaz. All rights reserved.
//

import Foundation

import RealmSwift
import RxSwift

/**
This protocol defines the interface for all database operations.
*/

public protocol Database: class {
  
  associatedtype DatabaseObject
  associatedtype DatabaseResult

  
  /**
  Fetch one or many objects of a type from the database
  */
  func get(_ objects: DatabaseObject.Type) -> DatabaseResult


}
