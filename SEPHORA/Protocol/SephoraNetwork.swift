//
//  Network.swift
//  SEPHORA
//
//  Created by Maher Ayari on 6/9/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import Foundation
import RxSwift

public protocol SephoraNetwork: class {
    associatedtype SephoraResponse

    func getItems() -> Observable<SephoraResponse>
}
