//
//  ItemDetailsViewModel.swift
//  SEPHORA
//
//  Created by Maher Ayari on 6/10/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import Foundation

class ItemDetailsViewModel: BaseViewModel {
    var item: Item
    
    required init(item: Item) {
      self.item = item
     }
}
