//
//  ItemDetailsViewController.swift
//  SEPHORA
//
//  Created by Maher Ayari on 6/10/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import UIKit

class ItemDetailsViewController: UIViewController {
    
    public var viewModel: ItemDetailsViewModel
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var itemDescription: UILabel!
    
    public init (viewModel: ItemDetailsViewModel){
        self.viewModel = viewModel
        super.init(nibName: String(describing: type(of: self)), bundle: nil)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = viewModel.item.location == "" ? "titre par defaut" : viewModel.item.location
        setupData()
    }
    
    private func setupData() {
        itemTitle.text =  viewModel.item.location == "" ? "titre par defaut" : viewModel.item.location
        itemDescription.text = viewModel.item.itemDescription == "" ? "description par defaut" : viewModel.item.itemDescription
    }
    
}
