//
//  ProductListViewController..swift
//  SEPHORA
//
//  Created by Maher Ayari on 06/08/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import UIKit
import RxSwift

class ProductListViewController: UIViewController {
    
    public var viewModel: ProductListViewModel
    public var delegate: ProductListTableViewDelegate!
    private var dataSource: ProductListTableViewDataSource!
    weak var ProductListCoordinator: ProductListCoordinator?
    
    @IBOutlet weak var tableView: UITableView!
    private let disposeBag = DisposeBag()
    
    
    public init(viewModel: ProductListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: String(describing: type(of: self)), bundle: nil)        
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Sephora"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupTableView()
        setupRx()
    }
    
    private func setupRx() {
        viewModel.dataManagerFactory.sephoraNetwork.send("qwe").subscribe(
            onNext: { items in
                self.viewModel.items = items
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }, onError: { error in
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Erro", message: "Network Error", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }).disposed(by: disposeBag)
    }
    
    private func setupTableView() {
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(ItemTableViewCell.self)
        
        // Datasource
        dataSource = ProductListTableViewDataSource(tableView: tableView, viewModel: viewModel)
        tableView.dataSource = dataSource
        // Delegate
        delegate = ProductListTableViewDelegate(tableView: tableView, viewModel: viewModel)
        tableView.delegate = delegate
        tableView.reloadData()
    }
    
}
