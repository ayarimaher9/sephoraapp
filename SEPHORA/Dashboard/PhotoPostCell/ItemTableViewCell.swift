//
//  PhotoTableViewCell.swift
//  SEPHORA
//
//  Created by Maher Ayari on 6/10/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemDescription: UILabel!
    @IBOutlet weak var postContent: UIView!
    @IBOutlet weak var itemImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configure(with item: Item) {
        postContent.layer.shadowColor = UIColor.gray.cgColor
        postContent.layer.shadowOpacity = 0.5
        postContent.layer.shadowOffset = .zero
        postContent.layer.shadowRadius = 2
        
        itemName.text =  item.location == "" ? "titre par defaut" : item.location
        itemDescription.text = item.itemDescription == "" ? "description par defaut" : item.itemDescription
    }
}
