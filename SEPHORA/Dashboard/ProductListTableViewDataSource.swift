//
//  ProductListTableViewDataSource.swift
//  SEPHORA
//
//  Created by Maher Ayari on 06/08/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import Foundation
import UIKit


class ProductListTableViewDataSource: NSObject, UITableViewDataSource {
    // MARK: - Properties
    private let tableView: UITableView!
    private let viewModel: ProductListViewModel
    
    
    // MARK: - Init
    init(tableView: UITableView, viewModel: ProductListViewModel) {
      self.tableView = tableView
      self.viewModel = viewModel
      super.init()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = viewModel.items[indexPath.row]
        let cell = tableView.dequeueReusable(ItemTableViewCell.self)
        cell.configure(with: item)
        return cell
    }
  
}
