//
//  ProductListViewModel.swift
//  SEPHORA
//
//  Created by Maher Ayari on 06/08/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import UIKit

class ProductListViewModel: BaseViewModel {
    
    public weak var productListDelegate: ProductListCoordinator?
    public let dataManagerFactory: DataManagerFactory
    public var items: [Item]

    public init(dataManagerFactory: DataManagerFactory) {
        self.dataManagerFactory = dataManagerFactory
        self.items = []
    }

    public func goToItemDetails(item: Item) {
        productListDelegate?.goToItemDetails(item: item)
    }
}
