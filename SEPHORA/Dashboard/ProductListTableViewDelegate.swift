//
//  ProductListTableViewDelegate.swift
//  SEPHORA
//
//  Created by Maher Ayari on 06/08/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import Foundation
import UIKit

class ProductListTableViewDelegate: NSObject, UITableViewDelegate {
    
    private let tableView: UITableView!
    private let viewModel: ProductListViewModel
    
    // MARK: - Init
    init(tableView: UITableView, viewModel: ProductListViewModel) {
      self.tableView = tableView
      self.viewModel = viewModel
      super.init()
    }
    
    // MARK: - UITableViewDelegate       
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      tableView.deselectRow(at: indexPath, animated: false)
        let item = viewModel.items[indexPath.row]
        viewModel.goToItemDetails(item: item)
}

}
