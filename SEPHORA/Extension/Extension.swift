//
//  Extension.swift
//  SEPHORA
//
//  Created by Maher Ayari on 6/10/21.
//  Copyright © 2021 SEPHORA. All rights reserved.
//

import UIKit

extension UITableView {
  func dequeueReusable<TVC>(_ tableViewCell: TVC.Type) -> TVC where TVC: UITableViewCell {
    guard let cell = dequeueReusableCell(withIdentifier: String(describing: tableViewCell.self)) as? TVC else {
      fatalError("Cannot create reusable cell with type \(String(describing: tableViewCell.self))). Did you register the cell for the table view? Did you set the cell identifier right?")
    }
    return cell
  }
  
  func register<TVC: UITableViewCell>(_ tableViewCell: TVC.Type) {
    register(UINib(nibName: String(describing: tableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: tableViewCell.self))
  }
}
